import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_shop/network/shared_helper.dart';
import 'package:my_shop/screens/home/home_screen.dart';
import 'package:my_shop/screens/intro/intro_screen.dart';
import 'package:my_shop/screens/login/login_screen.dart';
import 'package:my_shop/shared/myBlocObserver.dart';
import 'package:my_shop/shared/themes.dart';

import 'screens/login/login_cubit.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Bloc.observer = MyBlocObserver();
  await SharedHelper.init();

  bool endIntro = SharedHelper.getData(key: "intro") ;
  print("-------endIntro : $endIntro");
  String token = SharedHelper.getData(key: "token");
  print("------token is : $token");

  Widget widget;
  if (endIntro != null) {
    if (token != null) {
      widget = HomeScreen();
    } else {
      widget = LoginScreen();
    }
  } else {
    widget = IntroScreen();
  }

  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider<LoginCubit>(create: (context) => LoginCubit()),
      ],
      child: MyApp(startedWidget: widget),
    ),
  );
}

class MyApp extends StatelessWidget {
  final Widget startedWidget;

  const MyApp({Key? key, required this.startedWidget}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: lightTheme,
      home: startedWidget,
    );
  }
}
