class IntroModel {
  final String image;
  final String title;
  final String body;

  IntroModel(
    this.image,
    this.title,
    this.body,
  );
}
