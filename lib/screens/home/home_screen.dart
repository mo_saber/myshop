import 'package:flutter/material.dart';
import 'package:my_shop/network/shared_helper.dart';
import 'package:my_shop/screens/login/login_screen.dart';
import 'package:my_shop/shared/components.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: TextButton(
            child: Text("logout"),
            onPressed: () => SharedHelper.clearData(key: "token").then((value) {
              if (value) {
                navigateAndRemove(context, LoginScreen());
              }
            }),
          ),
        ),
      ),
    );
  }
}
