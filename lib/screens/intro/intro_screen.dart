import 'package:flutter/material.dart';
import 'package:my_shop/models/intro_model.dart';
import 'package:my_shop/network/shared_helper.dart';
import 'package:my_shop/screens/login/login_screen.dart';
import 'package:my_shop/shared/components.dart';
import 'package:my_shop/shared/myColors.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class IntroScreen extends StatefulWidget {
  const IntroScreen({Key? key}) : super(key: key);

  @override
  _IntroScreenState createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  List<IntroModel> introList = [
    IntroModel(
      "assets/images/welcome1.png",
      "Screen Title 1",
      "Screen Body 1",
    ),
    IntroModel(
      "assets/images/welcome2.png",
      "Screen Title 2",
      "Screen Body 2",
    ),
    IntroModel(
      "assets/images/welcome3.png",
      "Screen Title 3",
      "Screen Body 3",
    ),
  ];
  var introController = PageController();
  bool _isLast = false;

  void submit() {
    SharedHelper.setData(key: "intro", value: true).then((value) {
      navigateAndRemove(context, LoginScreen());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          _isLast == true
              ? Text("")
              : TextButton(
                  onPressed: () {
                    introController.animateToPage(
                      2,
                      duration: Duration(milliseconds: 700),
                      curve: Curves.fastLinearToSlowEaseIn,
                    );
                  },
                  child: Text(
                    "SKIP",
                    style: TextStyle(
                        color: primaryColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(25),
        child: Column(
          children: [
            Expanded(
              child: PageView.builder(
                controller: introController,
                physics: BouncingScrollPhysics(),
                itemCount: introList.length,
                itemBuilder: (context, index) => buildBoardingItems(
                  introList[index],
                ),
                onPageChanged: (index) {
                  if (index == introList.length - 1) {
                    print("-------------last page");
                    setState(() {
                      _isLast = true;
                    });
                  } else {
                    print("-------------not last page");
                    setState(() {
                      _isLast = false;
                    });
                  }
                },
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Row(
              children: [
                SmoothPageIndicator(
                  controller: introController,
                  count: introList.length,
                  effect: ExpandingDotsEffect(
                    dotColor: secondaryColor,
                    activeDotColor: primaryColor,
                    dotHeight: 10,
                    dotWidth: 10,
                    spacing: 5,
                    expansionFactor: 4,
                  ),
                ),
                Spacer(),
                FloatingActionButton(
                  onPressed: () {
                    if (_isLast) {
                      SharedHelper.setData(key: "intro", value: true)
                          .then((value) {
                        navigateAndRemove(context, LoginScreen());
                      });
                    } else {
                      introController.nextPage(
                        duration: Duration(milliseconds: 700),
                        curve: Curves.fastLinearToSlowEaseIn,
                      );
                    }
                  },
                  child: Icon(
                    Icons.arrow_forward_ios,
                    color: whiteColor,
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget buildBoardingItems(IntroModel introModel) {
    return Column(
      children: [
        Expanded(
          child: Image(
            image: AssetImage(introModel.image),
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          introModel.title,
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: 10,
        ),
        Text(introModel.body),
      ],
    );
  }
}
