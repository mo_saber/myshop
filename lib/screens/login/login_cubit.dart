import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_shop/models/login_model.dart';
import 'package:my_shop/network/dio_helper.dart';
import 'package:my_shop/network/end_points.dart';

import 'login_states.dart';

class LoginCubit extends Cubit<LoginStates> {
  LoginCubit() : super(LoginInitialState());

  static LoginCubit get(context) => BlocProvider.of(context);
  LoginModel loginModel = LoginModel();

  bool visible = true;

  void changePassword() {
    visible = !visible;
    emit(LoginChangePassState());
  }

  void loginUser({required String email, required String password}) {
    emit(LoginLoadingState());
    DioHelper.postData(url: LOGIN, data: {
      "email": email,
      "password": password,
    }).then((value) {
      loginModel = LoginModel.fromJson(value.data);
      print("------response is : ${value.data.toString()}");
      emit(LoginSuccessState(loginModel));
    }).catchError((error) {
      print("------error is : $error");
      emit(LoginErrorState(error.toString()));
    });
  }
}
