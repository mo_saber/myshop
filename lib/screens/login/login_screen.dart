import 'package:conditional_builder/conditional_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:my_shop/network/shared_helper.dart';
import 'package:my_shop/screens/home/home_screen.dart';
import 'package:my_shop/screens/login/login_states.dart';
import 'package:my_shop/screens/register/register_screen.dart';
import 'package:my_shop/shared/components.dart';
import 'package:my_shop/shared/myColors.dart';
import 'package:my_shop/shared/Validator.dart';

import 'login_cubit.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var emailController = TextEditingController();
    var passController = TextEditingController();
    var formKey = GlobalKey<FormState>();
    LoginCubit loginCubit = LoginCubit.get(context);

    return Scaffold(
      body: BlocConsumer<LoginCubit, LoginStates>(
        listener: (context, state) {
          if (state is LoginSuccessState) {
            if (state.loginModel.status = true) {
              showToast(
                  msg: state.loginModel.message ?? "", color: Colors.green);
              SharedHelper.setData(
                      key: "token", value: state.loginModel.data!.token)
                  .then((value) {
                print("-------token is : ${state.loginModel.data!.token}");
                navigateAndRemove(context, HomeScreen());
              });
            } else {
              showToast(msg: state.loginModel.message ?? "", color: Colors.red);
            }
          }
        },
        builder: (context, state) {
          return SingleChildScrollView(
            physics: BouncingScrollPhysics(
              parent: AlwaysScrollableScrollPhysics(),
            ),
            child: Form(
              key: formKey,
              child: Container(
                height: MediaQuery.of(context).size.height,
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "LOGIN",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 30,
                        color: primaryColor,
                      ),
                    ),
                    Text(
                      "Login now to browse our hot offers",
                      style: TextStyle(
                        color: secondaryColor,
                      ),
                    ),
                    SizedBox(height: 30),
                    TextFormField(
                      controller: emailController,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        labelText: "E-mail Address",
                        prefixIcon: Icon(Icons.email, color: primaryColor),
                        labelStyle: TextStyle(color: primaryColor),
                        border: OutlineInputBorder(),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: primaryColor,
                          ),
                        ),
                      ),
                      onFieldSubmitted: (value) {
                        print("email value is : $value");
                      },
                      validator: (value) => value!.validateEmail(),
                    ),
                    SizedBox(height: 10),
                    TextFormField(
                      controller: passController,
                      keyboardType: TextInputType.visiblePassword,
                      obscureText: loginCubit.visible,
                      decoration: InputDecoration(
                        labelText: "Password",
                        prefixIcon: Icon(Icons.lock, color: primaryColor),
                        suffixIcon: IconButton(
                          icon: Icon(
                            loginCubit.visible
                                ? Icons.visibility
                                : Icons.visibility_off,
                            color: primaryColor,
                          ),
                          onPressed: () => loginCubit.changePassword(),
                        ),
                        labelStyle: TextStyle(color: primaryColor),
                        border: OutlineInputBorder(),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: primaryColor,
                          ),
                        ),
                      ),
                      onFieldSubmitted: (value) {
                        print("password value is : $value");
                      },
                      validator: (value) => value!.validatePassword(),
                    ),
                    SizedBox(height: 30),
                    ConditionalBuilder(
                      condition: state is! LoginLoadingState,
                      builder: (context) {
                        return Container(
                          width: double.infinity,
                          color: primaryColor,
                          child: MaterialButton(
                            onPressed: () {
                              if (formKey.currentState!.validate()) {
                                loginCubit.loginUser(
                                    email: emailController.text,
                                    password: passController.text);
                              }
                            },
                            child: Text(
                              "LOGIN",
                              style: TextStyle(color: whiteColor),
                            ),
                          ),
                        );
                      },
                      fallback: (context) => Center(
                        child: SpinKitHourGlass(
                          color: primaryColor,
                          size: 50,
                        ),
                      ),
                    ),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("don\'t have an account ? "),
                        TextButton(
                          onPressed: () =>
                              navigateTo(context, RegisterScreen()),
                          child: Text(
                            "Register Now",
                            style: TextStyle(
                              color: primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
