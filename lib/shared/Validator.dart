extension Validator on String {
  String? noValidate() {
    return null;
  }

  String? validateEmpty() {
    if (this.trim().isEmpty) {
      return "please enter this Field";
    }
    return null;
  }

  String? validatePassword() {
    if (this.trim().isEmpty) {
      return "please enter this Field";
    } else if (this.length < 6) {
      return "password is too short";
    }
    return null;
  }

  String? validateEmail() {
    if (this.trim().isEmpty) {
      return "please enter this Field";
    } else if (!RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(this)) {
      return "please enter valid email";
    }
    return null;
  }

  String? validateEmailORNull() {
    if (this.trim().isNotEmpty) {
      if (!RegExp(
              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
          .hasMatch(this)) {
        return "please enter valid email";
      }
    }
    return null;
  }

  String? validatePhone() {
    if (this.trim().isEmpty) {
      return "please enter this Field";
    } else if (!RegExp(
                r'(^\+[0-9]{2}|^\+[0-9]{2}\(0\)|^\(\+[0-9]{2}\)\(0\)|^00[0-9]{2}|^0)([0-9]{9}$|[0-9\-\s]{10}$)')
            .hasMatch(this) ||
        this.length < 10) {
      return "please enter valid phone";
    }
    return null;
  }

  String? validatePasswordConfirm({required String pass}) {
    if (this.trim().isEmpty) {
      return "please enter this Field";
    } else if (this != pass) {
      return "passwords doesn't matched";
    }
    return null;
  }
}
