import 'package:flutter/material.dart';

const primaryColor = Colors.cyan;
const secondaryColor = Colors.blueGrey;
const whiteColor = Colors.white;
const blackColor = Colors.black;
