import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_shop/shared/myColors.dart';

ThemeData lightTheme = ThemeData(
  primarySwatch: primaryColor,
  scaffoldBackgroundColor: whiteColor,
  appBarTheme: AppBarTheme(
    backwardsCompatibility: false,
    systemOverlayStyle: SystemUiOverlayStyle(
      statusBarIconBrightness: Brightness.dark,
      statusBarColor: whiteColor,
    ),
    backgroundColor: whiteColor,
    elevation: 0.0,
    titleTextStyle: TextStyle(
      color: blackColor,
      fontWeight: FontWeight.bold,
      fontSize: 24,
    ),
  ),
  floatingActionButtonTheme: FloatingActionButtonThemeData(
    backgroundColor: primaryColor,
  ),
  fontFamily: "Jannah",
);
